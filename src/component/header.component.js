import {Component} from "../../core/component";

export class HeaderComponent extends Component
{
    constructor(id) {
        super(id)
    }

    init() {
        this.$el.textContent = 'this is component Header'
    }
}