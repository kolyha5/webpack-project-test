module.exports = {
    entry: {
        'index': './src/index.js',
        'second': './src/second.js'
    },
    output: {
        path: __dirname + '/dist',
        filename: '[name].js'
    },
    devServer: {
        contentBase: __dirname + '/dist'
    }
}